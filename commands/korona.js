const url = "https://koronavirus.gov.hu/";
const axios = require('axios');
const cheerio = require('cheerio');
const dayjs = require('dayjs');
require('dotenv').config();
module.exports = {
  name: '?korona',
  description: 'COVID-19 statistics',
  execute(msg, args, bot) {
      axios.get(url)
        .then(response => {

          let $ = cheerio.load(response.data);
          let fertozott_pest = $('#api-fertozott-pest')[0].children[0].data;
          let fertozott_videk = $('#api-fertozott-videk')[0].children[0].data;
          let gyogyult_pest = $('#api-gyogyult-pest')[0].children[0].data;
          let gyogyult_videk = $('#api-gyogyult-videk')[0].children[0].data;
          let elhunyt_pest = $('#api-elhunyt-pest')[0].children[0].data;
          let elhunyt_videk = $('#api-elhunyt-videk')[0].children[0].data;
          let quarantine = $('#api-karantenban')[0].children[0].data;
          let sample =  $('#api-mintavetel')[0].children[0].data;
          const message = {
          	color: 0xf3ff00,
          	title: 'Koronavírus állapota',
          	url: 'https://koronavirus.gov.hu',
          	description: 'Friss adatok a https://koronavirus.gov.hu weboldalról.',
            thumbnail: {
              url: 'https://res.cloudinary.com/d3v/image/upload/v1587498892/10481395_698757956863594_3975408923662716158_n_vyxtye.jpg'
            },
            fields: [
          		{
          			name: ':mask: Fertőzött (Pest - Vidék)',
          			value: fertozott_pest + " | " + fertozott_videk,
          			inline: true
          		},
          		{
          			name: ':+1: Gyógyult (Pest - Vidék)',
          			value: gyogyult_pest + " | " + gyogyult_videk,
          			inline: true
          		},
              {
          			name: ':skull: Elhunyt (Pest - Vidék)',
          			value: elhunyt_pest + " | " + elhunyt_videk,
          			inline: true
          		},
              {
          			name: ':biohazard: Karanténban',
          			value: quarantine,
          			inline: true
          		},
          		{
          			name: ':syringe: Mintavétel',
          			value: sample,
          			inline: true
          		}
          	],
          	timestamp: dayjs().format(),
          	footer: {
          		text: 'Generálva általam',
          		icon_url: process.env.avatarUrl,
          	},
          };

          msg.channel.send({ embed: message});
        })
        .catch(error => {
          console.log(error);
        })
  }
};
