const dayjs = require('dayjs');
require('dotenv').config();
exports.user = (msg) => {
  const error = {
    color: 0xff0000,
    title: 'Valami félrement',
    description: 'Nem valid user! Kérlek használt a @-ot az említéshez!',
    timestamp: dayjs().format(),
    footer: {
      text: 'Generálva általam',
      icon_url: process.env.avatarUrl,
    },
  };
  msg.channel.send({ embed: error })
}
exports.dm = (msg) => {
  const error = {
    color: 0xff0000,
    title: 'Valami félrement',
    description: 'Ezt a kommandot csak szerver csatornán tudom végrehajtani!',
    timestamp: dayjs().format(),
    footer: {
      text: 'Generálva általam',
      icon_url: process.env.avatarUrl,
    },
  };
  msg.channel.send({ embed: error })
}
exports.id = (msg) => {
  const error = {
    color: 0xff0000,
    title: 'Valami félrement',
    description: 'ID nélkül mit keressek? Tűt a széna kazalban?',
    timestamp: dayjs().format(),
    footer: {
      text: 'Generálva általam',
      icon_url: process.env.avatarUrl,
    },
  };
  msg.channel.send({ embed: error })
}
exports.text = (msg) => {
  const error = {
    color: 0xff0000,
    title: 'Valami félrement',
    description: 'Nem adtál meg semmi szöveget, így nincsen több teendőm!',
    timestamp: dayjs().format(),
    footer: {
      text: 'Generálva általam',
      icon_url: process.env.avatarUrl,
    },
  };
  msg.channel.send({ embed: error })
}
exports.userNotConnectedVoiceChannel = (msg) => {
  const error = {
    color: 0xff0000,
    title: 'Valami félrement',
    description: 'Ha nem vagy fent voice chat-en, akkor nem tudom hová bejátszani a kérésed!',
    timestamp: dayjs().format(),
    footer: {
      text: 'Generálva általam',
      icon_url: process.env.avatarUrl,
    },
  };
  msg.channel.send({ embed: error })
}
