const dayjs = require('dayjs');
const fs = require('fs');
const path = require('path');
const errors = require('./errors_templates');
require('dotenv').config();
module.exports = {
  name: '?v',
  description: 'Join a voice channel and play some effect!',
  execute(msg, args, bot) {

    const message = {
      color: 0x0099ff,
      title: 'Voice kommandok',
      fields: [

      ],
      timestamp: dayjs().format(),
      footer: {
        text: 'Generálva általam',
        icon_url: process.env.avatarUrl,
      },
    };

    switch (args[0]) {
      case '1':
         message.fields[0] = {
          name: 'Az alábbi kommandok elérhetőek jelenleg:',
          value: '\n`?v csepereg` - Egy meghatározó szám az életünkben' +
            '\n`?v hello` - Egy kedves elköszönés' +
            '\n`?v szevasztok` - Egy kedves köszönés' +
            '\n`?v mire` - Egy udvarias fiatalember kijelentése' +
            '\n`?v gyere` - Egy test található szerv elhellyezésére tett kisérlet' +
            '\n`?v légióban` - A régi hadban töltött emlékek felidézésére' +
            '\n`?v neparancsolgassá` - Tiszteletteljes felkérés' +
            '\n`?v 5k` - Szervezett, nagy mennyiségű nitroglicerin és abszorbens kovaföld keverék szállítása' +
            '\n`?v kisköcsög` - Torrente' +
            '\n`?v zsidó` - Mi a need zsió?' +
            '\n`?v biblia` - Hittudományin tanító tanár' +
            '\n`?v blast` - Garantált süketüllés' +
            '\n`?v hülyegyerek` - Torrente' +
            '\n`?v ee` - ee eee eeeeee eeee eeeeee' +
            '\n`?v nice` - Szerintem, ezt nem kell ecsetelnem' +
            '\n`?v ok` - Egy kedves néger úriember válasza' +
            '\n`?v 9000` - It\'s over 9000' +
            '\n`?v dial` - Egy régi modem' +
            '\n`?v doit` - Egy bölcs régi mondása' +
            '\n`?v error` - Windows error' +
            '\n`?v fatality` - Ezt sem kell elmagyaráznom' +
            '\n`?v illuminati` - Ez veszélyes' +
            '\n'
        }
        return msg.channel.send({ embed: message });
      case '2':
        message.fields[0] =
        {
          name: 'Az alábbi kommandok elérhetőek jelenleg:',
          value: '\n`?v john` - Egy kis testű birkózó' +
            '\n`?v moments` - ' +
            '\n`?v rip` - ' +
            '\n`?v spagheti` - ' +
            '\n`?v tada` - ' +
            '\n`?v titanic` - ' +
            '\n`?v bela` - Béla' +
            //'\n`?v` - ' +
            '\n'
        }
        return msg.channel.send({ embed: message });
      case 'líe':
      case 'lie':
        voiceCheck(msg,'korszeru.mp3');
        break;
      case 'csepereg':
        voiceCheck(msg,'csepereg.mp3')
        break;
      case 'helló':
      case 'hello':
        voiceCheck(msg,'hello.mp3')
        break;
      case 'szevasztok':
        voiceCheck(msg,'szevasztok.mp3')
        break;
      case 'míre':
      case 'mire':
        voiceCheck(msg,'mire.mp3')
        break;
      case 'gyere':
        voiceCheck(msg,'gyere.mp3')
        break;
      case 'legíoban':
      case 'legioban':
      case 'légióban':
        voiceCheck(msg,'légióban.mp3')
        break;
      case 'neparancsolgassa':
      case 'neparancsolgassá':
        voiceCheck(msg,'neparancsolgassá.mp3')
        break;
      case 'bomba':
      case '5k':
        voiceCheck(msg,'5k.mp3')
        break;
      case 'kískocsog':
      case 'kiskocsog':
      case 'kisköcsög':
        voiceCheck(msg,'kisköcsög.mp3')
        break;
      case 'zsído':
      case 'zsido':
      case 'zsidó':
        voiceCheck(msg,'zsido.mp3')
        break;
      case 'bíblía':
      case 'biblia':
        voiceCheck(msg,'biblia.mp3')
        break;
      case 'blast':
        voiceCheck(msg,'blast.mp3')
        break;
      case 'hulyegyerek':
      case 'hülyegyerek':
        voiceCheck(msg,'hülyegyerek.mp3')
        break;
      case 'ee':
        voiceCheck(msg,'ee.mp3')
        break;
      case 'nice':
        voiceCheck(msg,'nice.mp3')
        break;
      case 'ok':
        voiceCheck(msg,'ok.mp3')
        break;
      case '9k':
      case '9000':
        voiceCheck(msg,'9000.mp3')
        break;
      case 'dial':
        voiceCheck(msg,'dial.mp3')
        break;
      case 'do it':
      case 'doit':
        voiceCheck(msg,'doit.mp3')
        break;
      case 'error':
        voiceCheck(msg,'error.mp3')
        break;
      case 'fatality':
        voiceCheck(msg,'fatality.mp3')
        break;
      case 'illuminati':
        voiceCheck(msg,'illuminati.mp3')
        break;
      case 'john':
        voiceCheck(msg,'john.mp3')
        break;
      case 'moments':
        voiceCheck(msg,'moments.mp3')
        break;
      case 'rip':
        voiceCheck(msg,'rip.mp3')
        break;
      case 'spagheti':
        voiceCheck(msg,'spagheti.mp3')
        break;
      case 'tada':
        voiceCheck(msg,'tada.mp3')
        break;
      case 'titanic':
        voiceCheck(msg,'titanic.mp3')
        break;
      case 'bela':
        voiceCheck(msg,'bela.mp3')
        break;
      default:
        message.fields[0] = {
            name: 'Az alábbi kommandok elérhetőek jelenleg:',
            value: '\n`?v 1` - Help első oldal' +
              '\n`?v 2` - Help második oldal' +
              '\n'
          }
        return msg.channel.send({ embed: message });


      }
  }
};

function voiceCheck(msg, sound){
  const voiceChannel = msg.member.voice.channel;
  if (!voiceChannel) errors.userNotConnectedVoiceChannel(msg)
  else play(msg, sound);
}

async function play(msg, sound) {
  const connection = await msg.member.voice.channel.join();
  const dispatcher = connection.play(fs.createReadStream(path.join(__dirname, '..', 'sounds', sound)));
  dispatcher.on('start', () => {});

  dispatcher.on('finish', () => {
    connection.disconnect();
  });
  dispatcher.on('error', console.error);
}
