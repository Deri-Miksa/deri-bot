var giphy = require('giphy-api')(process.env.giphy_api);
const errors = require('./errors_templates');
module.exports = {
  name: '?random',
  description: 'Random!',
  execute(msg, args, bot) {
    if (args[0]){
      args.forEach(async function (item) {
        giphy.random(item).then(function (res) {
          msg.channel.send(res.data.image_mp4_url);
        });
      });
    }
    else {
      errors.text(msg);
    }
  },
};
