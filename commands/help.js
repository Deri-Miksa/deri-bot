const dayjs = require('dayjs');
require('dotenv').config();
module.exports = {
  name: '?help',
  description: 'Help',
  execute(msg, args, bot) {
    const exampleEmbed = {
      color: 0x0099ff,
      title: 'Információ',
      url: 'https://derimiksa.hu',
      fields: [
        {
          name: 'Kedves Barátom!',
          value: 'Tudom, hogy most nehéz idő jár ránk eme fránya fertőzéshullám miatt.' +
          '\nNem fogok beleavatkozni a társalgásukba, csak külső szemlélődő leszek.' +
          '\nAmennyiben szeretnék igénybe venni a segítségemet, ahhoz szükséges kommandokat kell megadni:',
        },
        {
          name: '\u200b',
          value: '\u200b',
          inline: false,
        },
        {
          name: 'A topic helyet beírsz egy általad válaszott témát és random küld egy gif-et',
          value: '`?random <téma>`',
          inline: true,
        },
        {
          name: 'Lekéri a jelenlegi statisztikákat a magyar helyzettel kapcsolatban',
          value: '`?korona`',
          inline: true,
        },
        {
          name: 'Különböző effektek bejátszása az adott voice channel-en',
          value: '`?v`',
          inline: true,
        },
        {
          name: 'Elérhetőség tesztelése és késleltetés mérése',
          value: '`?ping`',
          inline: true,
        },
        {
          name: 'Információk lekérése a bot segítségével',
          value: '`?info`',
          inline: true,
        },
        {
          name: 'Csatornán lévő üzenetek törlése',
          value: '`?clear <szám>`',
          inline: true,
        },
        {
          name: 'Az alábbi üzenetet jeleníti meg',
          value: '`?help`',
          inline: true,
        },
        {
          name: 'Fejlesztői opciók',
          value: '`?dev`',
          inline: true,
        },

        {
          name: '\u200b',
          value: '\u200b',
          inline: false,
        },
        {
          name: 'Támogatás',
          value: '[Ha tetszenek a funkcióim, akkor kérlek támogasd egy kávéval a készítőmet.](https://www.buymeacoffee.com/d3v \'Támogass egy kávéval\')',
          inline: false,
        }
      ],
      timestamp: dayjs().format(),
      footer: {
        text: 'Generálva általam',
        icon_url: process.env.avatarUrl,
      }
    };

    msg.channel.send({ embed: exampleEmbed });
  },
};
