const dayjs = require('dayjs');
const errors = require('./errors_templates');
const WATSON_LANG_ENDPOINT = "https://api.eu-de.language-translator.watson.cloud.ibm.com/instances/1ae81104-a38a-4e42-859d-7adb1a1b5d77/v3/translate?version=2018-05-01";
const WATSON_LANG_API_KEY = "sfM6NjipVDhPzdN6noCaHgW4XS-51vIYuPgBm4Q_WoFS";
const WATSON_ASSISSTANT_API_KEY = "QzZWG-2bqRigbLtNDyeIzL8bbR-pHpJNQ9KQQ6qTLgO6"

const DiscoveryV1 = require('ibm-watson/discovery/v1');
const { IamAuthenticator } = require('ibm-watson/auth');

const discoveryClient = new DiscoveryV1({
  authenticator: new IamAuthenticator({ apikey: WATSON_ASSISSTANT_API_KEY }),
  version: '2018-04-15',
});
discoveryClient.setServiceUrl('https://api.eu-de.assistant.watson.cloud.ibm.com/instances/b6a451a3-0896-44dd-a0e2-0fda7d2da730');

const axios = require('axios');

require('dotenv').config();
module.exports = {
  name: '?t',
  description: 'Development option',
  execute(msg, args, bot) {
    let question = msg.content.split("?t ")[1];

    axios({
      method: 'post',
      url: WATSON_LANG_ENDPOINT,
      auth: {
        username: "apikey",
        password: WATSON_LANG_API_KEY
      },
      headers:{
        "Content-Type": "application/json"
      },
      data: {
        text: question,
        model_id: "hu-en"
      }
    }).then(response => {
      let translation = response.data.translations[0].translation;
      msg.channel.send(translation);
    })
  }
}
