const dayjs = require('dayjs');
require('dotenv').config();
module.exports = {
  name: '?poll',
  description: 'poll',
  execute(msg, args, bot) {

      switch (args[0]) {
        case 'ban':
          break;
        case 'mute':
          let user = args[1].replace(/[^\w\s]/gi, '');
          if (!isNaN(Number(user))) {
            const message = {
              color: 0x7cfc00,
              title: 'Új szavazás',
              description: '<@' + msg.author.id + '> szavazást indított!',
              author: {
            		name: msg.author.username,
            		icon_url: msg.author.displayAvatarURL({ format: 'jpg' })
            	},
              thumbnail: {
            		url: msg.guild.members.cache.get(user).user.displayAvatarURL({ format: 'jpg' })
            	},
              fields: [
                {
                  name: '\u200b',
                  value: '\u200b',
                  inline: false,
                },
            		{
            			name: 'Támadott felhasználó',
            			value: '<@' + user + '>',
                  inline: false
            		},
                {
            			name: 'Büntetés',
            			value: 'Voice chat force mute',
                  inline: true
            		},
              ],
              timestamp: dayjs().format(),
              footer: {
                text: 'Generálva általam',
                icon_url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/D%C3%A9ri_Miksa.jpg/250px-D%C3%A9ri_Miksa.jpg',
              },
            };
            msg.channel.send({ embed: message})
              .then((sent) => {
                sent.react("👍");
                sent.react("👎");
              })
              .then(() =>{
                msg.channel.fetch({ limit: 1 }).then((last) => {
                  const verify = {
                    color: 0x7cfc00,
                    title: 'Elfogadtatás',
                    description: 'A szavazás lezárásához az alábbi kommand:',

                    fields: [
                      {
                        name: '\u200b',
                        value: '`?poll sum ' + last.lastMessageID + '`',
                        inline: false,
                      },
                    ],
                    timestamp: dayjs().format(),
                    footer: {
                      text: 'Generálva általam',
                      icon_url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/D%C3%A9ri_Miksa.jpg/250px-D%C3%A9ri_Miksa.jpg',
                    },
                  };
                  msg.channel.send({embed: verify})
                });
              });
          } else {
            const message = {
              color: 0xff0000,
              title: 'Valami félrement',
              description: 'Nem valid user!',
              timestamp: dayjs().format(),
              footer: {
                text: 'Generálva általam',
                icon_url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/D%C3%A9ri_Miksa.jpg/250px-D%C3%A9ri_Miksa.jpg',
              },
            };
            msg.channel.send({ embed: message})
          }
          // if (args[2]){
          //   message.fields[3] = {
          // 			name: 'Ok',
          // 			value: (args.toString().replace(args[0]+',','').replace(args[1],'').replace(args[1]+',','').replace(',',' ')),
          //       inline: true
          //   }
          // }

          break;
        case 'op':
          break;
        case 'deop':
          break;
        case 'sum':
          let poll_id = args[1];
          let all_user = msg.channel.members.size;
          msg.channel.messages.fetch({ around: poll_id, limit: 1 }).then(poll_content => {
             console.log(poll_content.get(poll_id).reactions.cache);
             let up = poll_content.get(poll_id).reactions.cache.get('👍').count
             let down = poll_content.get(poll_id).reactions.cache.get('👎').count
             console.log('UP:', up,'DOWN:',down);
          });

          break;
      }
  }
};
