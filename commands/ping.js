const dayjs = require('dayjs');
require('dotenv').config();
module.exports = {
  name: '?ping',
  description: 'Measure latency',
  execute(msg, args, bot) {
    const message = {
      color: 0xffc300,
      title: 'Pinging...',
      fields: [
        {
          name: 'Késleltetés',
          value: '** ... ms**',
          inline: false
        }
      ],
      timestamp: dayjs().format(),
      footer: {
        text: 'Generálva általam',
        icon_url: process.env.avatarUrl,
      },
    };
    msg.channel.send({ embed: message}).then( (sent) => {
      message.title = 'Pong';
      message.color = 0x7cfc00;
      message.fields[0] =
      {
        name: 'Késleltetés',
        value: '**' + (sent.createdTimestamp - msg.createdTimestamp) + 'ms**',
        inline: false
      };
      sent.edit({ embed: message});
    });
  },
};
