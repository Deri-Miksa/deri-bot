const dayjs = require('dayjs');
const errors = require('./errors_templates');
require('dotenv').config();
module.exports = {
  name: '?info',
  description: 'Base informations from received json',
  execute(msg, args, bot) {
    const message = {
      fields: [

      ],
      timestamp: dayjs().format(),
      footer: {
        text: 'Generálva általam',
        icon_url: process.env.avatarUrl,
      },
    };
    switch (args[0]) {
      case 'server':
        message.color = 0x7cfc00;
        message.title = 'Információ';
        if (msg.channel.type != "dm") {
          let guild = msg.guild;
          message.description = 'Pár adat a szerverrel kapcsolatban';
          message.fields[1] =
          {
            name: 'Szerver id',
            value: format('b', guild.id),
            inline: false
          };
          message.fields[2] =
          {
            name: 'Shard ID',
            value: format('b', guild.shardID),
            inline: true
          };
          message.fields[3] =
          {
            name: 'Régió',
            value: format('b', guild.region),
            inline: true
          };
          message.fields[4] =
          {
            name: 'Tagok száma',
            value: format('b', guild.memberCount),
            inline: true
          };
          message.fields[5] =
          {
            name: 'Nagy szerver',
            value: format('b', guild.large),
            inline: true
          };
          message.fields[6] =
          {
            name: 'AFK idő',
            value: format('b', guild.afkTimeout),
            inline: true
          };
          if (msg.guild.afkChannelID != null)
          {
            message.fields[7] =
            {
              name: 'AFK csatorna',
              value: format('c', guild.afkChannelID),
              inline: true
            };
          };
          message.fields[8] =
          {
            name: 'Fő csatorna',
            value: format('c', guild.systemChannelID),
            inline: true
          };
          message.fields[9] =
          {
            name: 'Prémium tier',
            value: format('b', guild.premiumTier),
            inline: true
          };
          message.fields[10] =
          {
            name: 'Ellenőrzési szint',
            value: format('b', guild.verificationLevel),
            inline: true
          };
          message.fields[11] =
          {
            name: 'Explicit kontent filter',
            value: format('b', guild.explicitContentFilter),
            inline: true
          };
          message.fields[12] =
          {
            name: 'Szerver létrehozása',
            value: format('b',dayjs(guild.joinedTimestamp).format('YYYY-MM-DD')),
            inline: true
          };
          message.fields[13] =
          {
            name: 'Tulajdonos',
            value: format('u',guild.ownerID),
            inline: true
          };

          // message.fields[] =
          // {
          //   name: '',
          //   value: '',
          //   inline: true
          // };


          msg.channel.send({ embed: message})
        } else {
          errors.dm(msg);
        }
        break;
      case 'me':
        let author = msg.author;
        message.color = 0x7cfc00;
        message.title = 'Információ';
        message.description = 'Pár adat a rólad';
        message.thumbnail = {
          url: author.displayAvatarURL({ format: 'jpg' })
        };
        message.fields[1] =
        {
          name: 'Felhasználónév',
          value: format('b', author.username),
          inline: true
        };
        message.fields[2] =
        {
          name: 'Felhasználói id',
          value: format('b', author.id),
          inline: true
        };
        message.fields[3] =
        {
          name: 'Felhasználói azonosító',
          value: format('b', author.discriminator),
          inline: true
        };
        message.fields[4] =
        {
          name: 'Utolsó üzenet id',
          value: format('b', author.lastMessageID),
          inline: true
        };
        message.fields[5] =
        {
          name: 'Utolsó látogatott csatorna',
          value: format('b', author.lastMessageChannelID),
          inline: true
        };
        message.fields[6] =
        {
          name: 'Bot',
          value: format('b', author.bot),
          inline: true
        };
        msg.author.send({ embed: message})
        break;
      case 'user':
        if (msg.channel.type != "dm") {
          let user_id = args[1].replace(/[^\w\s]/gi, '');
          let user_data = msg.guild.members.cache.get(user_id).user;
          if (!isNaN(Number(user_id))){
            message.color = 0x7cfc00;
            message.title = 'Információ';
            message.description = 'Adatok a kért felhasználóról';
            message.thumbnail = {
              url: user_data.displayAvatarURL({ format: 'jpg' })
            };
            message.fields[1] =
            {
              name: 'Felhasználónév',
              value: format('b', user_data.username),
              inline: true
            };
            message.fields[2] =
            {
              name: 'Felhasználói id',
              value: format('b', user_data.id),
              inline: true
            };
            message.fields[3] =
            {
              name: 'Felhasználói azonosító',
              value: format('b', user_data.discriminator),
              inline: true
            };
            if (!user_data.bot) {
              message.fields[4] =
              {
                name: 'Utolsó üzenet id',
                value: format('b', user_data.lastMessageID),
                inline: true
              };
              message.fields[5] =
              {
                name: 'Utolsó látogatott csatorna',
                value: format('b', user_data.lastMessageChannelID),
                inline: true
              };
            }
            message.fields[6] =
            {
              name: 'Bot',
              value: format('b', user_data.bot),
              inline: true
            };
            msg.author.send({ embed: message})
          } else {
            errors.user(msg);
          }
        } else {
          errors.dm(msg);
        }
        break;
      case 'bot':
        if (msg.author.id == '342836263541276673'){
          message.color = 0xff5733;
          message.title = 'Fejlesztői információ';
          message.fields[1] =
          {
            name: 'Szerverek neve',
            value: format('b', bot.guilds.cache.map( u => u.name).join("\n")),
            inline: true
          };
          message.fields[2] =
          {
            name: 'Szerverek id-ja',
            value: format('b', bot.guilds.cache.map( u => u.id).join("\n")),
            inline: true
          };
          message.fields[3] =
          {
            name: 'Szerverek régiói',
            value: format('b', bot.guilds.cache.map( u => u.region).join("\n")),
            inline: true
          };
          message.fields[4] =
          {
            name: 'Felhasználók száma',
            value: format('b', bot.guilds.cache.map( u => u.memberCount).join("\n")),
            inline: true
          };
          message.fields[5] =
          {
            name: 'Szerverek tulajdonosai',
            value: format('b', bot.guilds.cache.map( u => `<@${u.ownerID}>`).join("\n")),
            inline: true
          };

          msg.author.send({ embed: message });
        } else {
          let user_data = msg.guild.members.cache.get('690545557051277342').user;
          message.color = 0x7cfc00;
          message.title = 'Információ';
          message.thumbnail =
          {
            url: user_data.displayAvatarURL({ format: 'jpg' })
          };
          message.fields[1] =
          {
            name: 'Felhasználónév',
            value: format('b', user_data.username),
            inline: true
          };
          message.fields[2] =
          {
            name: 'Felhasználói id',
              value: format('b', user_data.id),
            inline: true
          };
          message.fields[3] =
          {
            name: 'Felhasználói azonosító',
            value: format('b', user_data.discriminator),
            inline: true
          };
          message.fields[4] =
          {
            name: 'Szerverek száma, melyek használnak',
            value: format('b', bot_guilds.length),
            inline: true
          };
          msg.channel.send({ embed: message })
        }
        break;
      default:
        message.color = 0x7cfc00;
        message.title = 'Információ';
        message.fields[0] =
        {
          name: 'Információ magadról',
          value: format('cc', '?info me'),
          inline: true
        };
        message.fields[1] =
        {
          name: 'Információ másik felhasználóról',
          value: format('cc', '?info user <@Felhasználó említése>'),
          inline: true
        };
        message.fields[2] =
        {
          name: 'Információ a szerverről',
          value: format('cc', '?info server'),
          inline: true
        };
        message.fields[3] =
        {
          name: 'Információ a a botról',
          value: format('cc', '?info bot'),
          inline: true
        };
        msg.channel.send({ embed: message })
        break;
    }

  }
};

function format(style,text){
  switch (style) {
    case 'b':
      return '**' + text + '**';
    case 'u':
      return '<@' + text + '>';
    case 'c':
      return '<#' + text + '>';
    case 'cc':
      return '`' + text + '`';

  }
}
