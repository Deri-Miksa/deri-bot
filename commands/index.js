module.exports = {
  Ping: require('./ping'),
  Random: require('./random'),
  Korona: require('./korona'),
  Help: require('./help'),
  Voice: require('./voice'),
  Clear: require('./clear'),
  //Poll: require('./poll'),
  Info: require('./info'),
  Dev: require('./dev'),
  Talk: require('./talk')
};
