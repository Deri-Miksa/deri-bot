const dayjs = require('dayjs');
const errors = require('./errors_templates');
require('dotenv').config();
module.exports = {
  name: '?dev',
  description: 'Development option',
  execute(msg, args, bot) {
    const message = {
      fields: [],
      timestamp: dayjs().format(),
      footer: {
        text: 'Generálva általam',
        icon_url: process.env.avatarUrl,
      },
    };

    if (msg.author.id == '342836263541276673'){
      switch (args[0]) {
        case 'guilds':
          message.color = 0xff5733;
          message.title = 'Guilds';
          message.description = 'Szerverek melyre meghívták a botot'
          message.fields[0] =
          {
            name: 'Szerver neve',
            value: format('b', bot.guilds.cache.map( u => u.name).join("\n")),
            inline: true
          };
          message.fields[1] =
          {
            name: 'Szerver id-ja',
            value: format('b', bot.guilds.cache.map( u => u.id).join("\n")),
            inline: true
          };
          message.fields[2] =
          {
            name: 'Szerver tulajdonosa',
            value: format('b', bot.guilds.cache.map( u => `<@${u.ownerID}>`).join("\n")),
            inline: true
          };
          msg.author.send({ embed: message });
          break;
        case 'channels':
          if (!isNaN(Number(args[1]))){
            message.color = 0xff5733;
            message.title = 'Csatornák';
            message.description = 'A ' + format('b', bot.guilds.cache.get(args[1]).name) + ' szerveren található csatornák'
            message.fields[0] =
            {
              name: 'Csatornák',
              value: format('b', bot.guilds.cache.get(args[1]).channels.cache.map(u => u.name).join("\n")),
              inline: true
            };
            message.fields[1] =
            {
              name: 'Típus',
              value: format('b', bot.guilds.cache.get(args[1]).channels.cache.map(u => u.type).join("\n")),
              inline: true
            };
            message.fields[2] =
            {
              name: 'Típus',
              value: format('b', bot.guilds.cache.get(args[1]).channels.cache.map(u => u.id).join("\n")),
              inline: true
            };

            msg.author.send({ embed: message });
          } else {
            errors.id(msg);
          }
          break;
        case 'users':
          if (!isNaN(Number(args[1]))){
            message.color = 0xff5733;
            message.title = 'A ' + format('b', bot.guilds.cache.get(args[1]).name) + ' szerveren lévő felhasználók';
            message.fields[0] =
            {
              name: 'Felhasználók',
              value: bot.guilds.cache.get(args[1]).members.cache.map(u => u.user).join("\n"),
              inline: true
            };

            msg.author.send({ embed: message });
          } else {
            errors.id(msg);
          }
          break;
        case 'send':
          if (!isNaN(Number(args[1])) && !isNaN(Number(args[2]))){
            bot.guilds.cache.get(args[1]).channels.cache.get(args[2]).send(args.slice(3).join(' '));
            message.color = 0x7cfc00;
            message.title = 'Üzenet elküdve';
            msg.author.send({ embed: message });
          } else {
            errors.id(msg);
          }

          break;
        case 'status':
          if (!isNaN(Number(args[1]))) {
            bot.user.setActivity(args.slice(1).join(' '));

          } else {
            errors.text(msg);
          }
          break;
        case 'maintain':
          if (args[1] && args[2]){
            message.color = 0xff0000;
            message.title = 'Szerver tulajdonosok éresítése a szolgáltatás leállásáról';
            message.description = 'A kieséssel járó kellemetlenségekért szíves elnézésüket kérjük!'
            message.fields[0] =
            {
              name: 'OK',
              value: args.slice(3).join(' '),
              inline: false
            };
            message.fields[1] =
            {
              name: 'Szolgáltatás újból elérhető',
              value: dayjs(args[1]+args[2]).format('YYYY-MM-DD HH:mm'),
              inline: false
            };
            message.footer.icon_url = msg.author.displayAvatarURL({ format: 'jpg' });
            let owners = new Set(bot.guilds.cache.map(u => u.ownerID));
            owners.forEach((item, i) => {
              bot.users.cache.get(item).send({embed: message});
            });
          } else {
            errors.text(msg);
          }
          break;
        default:
          message.color = 0x7cfc00;
          message.title = 'Parancsok';
          message.fields[0] =
          {
            name: 'Szerverek',
            value: format('cc', '?dev guilds'),
            inline: true
          };
          message.fields[1] =
          {
            name: 'Szerveren lévő csatornák',
            value: format('cc', '?dev channels <guild-id>'),
            inline: true
          };
          message.fields[2] =
          {
            name: 'Üzenet a szerver csatornájára',
            value: format('cc', '?dev send <guild-id> <channel-id> <message>'),
            inline: true
          };
          message.fields[3] =
          {
            name: 'Szerveren lévő felhasználók',
            value: format('cc', '?dev users <guild-id>'),
            inline: true
          };
          message.fields[4] =
          {
            name: 'Leállással kapcsolatos üzenet a szerver tulajdonosoknak',
            value: format('cc', '?dev maintain <year-month-day> <hour:minute> <reason>'),
            inline: true
          };

          msg.author.send({ embed: message });
          break;
      }

    }
  }
};

function format(style,text){
  switch (style) {
    case 'b':
      return '**' + text + '**';
    case 'u':
      return '<@' + text + '>';
    case 'c':
      return '<#' + text + '>';
    case 'cc':
      return '`' + text + '`';

  }
}
