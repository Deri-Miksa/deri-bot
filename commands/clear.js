const dayjs = require('dayjs');
require('dotenv').config();
module.exports = {
  name: '?clear',
  description: 'Clear the last x number messages.',
  execute(msg, args, bot) {
      if (!isNaN(Number(args[0]))){
        if (msg.channel.permissionsFor(msg.member).has("MANAGE_MESSAGES", false)){
          msg.channel.bulkDelete(Number(args[0]));
        }
        else {
          const message = {
            color: 0xff0000,
            title: 'Valami félrement',
            description: 'Sajnos a parancs végrehajtására nincsen privilégiumod (MANAGE_MESSAGES)!',
            timestamp: dayjs().format(),
            footer: {
              text: 'Generálva általam',
              icon_url: process.env.avatarUrl,
            },
          };
          msg.channel.send({ embed: message });
        }

      }
      else {
        const message = {
          color: 0xff0000,
          title: 'Valami félrement',
          description: 'Egy számot lenne érdemes megadnod, mert így nem tudom, hogy mit törölhetek ki.',
          timestamp: dayjs().format(),
          footer: {
            text: 'Generálva általam',
            icon_url: process.env.avatarUrl,
          },
        };
        msg.channel.send({ embed: message });
      }
  },
};
