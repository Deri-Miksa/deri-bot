const mongoose = require("mongoose");

const WeekSchema = mongoose.Schema({
  "student_id": {
    "type": "ObjectId"
  },
  "notified":{
    "type": "Bool"
  },
  "details": {
    "LessonId": {
      "type": "Number"
    },
    "CalendarOraType": {
      "type": "String"
    },
    "Count": {
      "type": "Number"
    },
    "Date": {
      "type": "Date"
    },
    "StartTime": {
      "type": "Date"
    },
    "EndTime": {
      "type": "Date"
    },
    "Subject": {
      "type": "String"
    },
    "SubjectCategory": {
      "type": "String"
    },
    "SubjectCategoryName": {
      "type": "String"
    },
    "ClassRoom": {
      "type": "String"
    },
    "OsztalyCsoportId": {
      "type": "Number"
    },
    "ClassGroup": {
      "type": "String"
    },
    "Teacher": {
      "type": "String"
    },
    "DeputyTeacher": {
      "type": "String"
    },
    "State": {
      "type": "String"
    },
    "StateName": {
      "type": "String"
    },
    "PresenceType": {
      "type": "String"
    },
    "PresenceTypeName": {
      "type": "String"
    },
    "TeacherHomeworkId": {
      "type": "Number"
    },
    "IsTanuloHaziFeladatEnabled": {
      "type": "Boolean"
    },
    "Nev": {
      "type": "String"
    }
  },
  "create_at": {
    "type": "Date"
  }
});

// export model user with UserSchema
module.exports = mongoose.model("week", WeekSchema);
