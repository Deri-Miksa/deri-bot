FROM node:13.10.1

ENV TZ=Europe/Budapest
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt update -y \
  && apt install ffmpeg -y

WORKDIR /usr/src/app
COPY . .
RUN npm install

CMD [ "node", "app.js" ]
