require('dotenv').config();
const Discord = require('discord.js');
const bot = new Discord.Client();
bot.commands = new Discord.Collection();
const chalk = require('chalk');
const express = require('express');
const app = express();
const path = require('path');
const port = process.env.PORT || 3000;
const dayjs = require('dayjs');
const botCommands = require('./commands');

const error = chalk.bold.red;
const warning = chalk.keyword('orange');
console.clear();
Object.keys(botCommands).map(key => {
  bot.commands.set(botCommands[key].name, botCommands[key]);
});

const TOKEN = process.env.TOKEN;

bot.login(TOKEN);

bot.on('ready', () => {
  console.info('Logged in as ' + chalk.green(bot.user.tag));
  bot.user.setActivity("Próbálom teljesíteni a kéréseiteket! Rabszolga munka...");
});

bot.on('message', (msg) => {
  if (msg.content.startsWith('?') && !msg.author.bot){
    const args = msg.content.split(/ +/);
    const command = args.shift().toLowerCase();
    console.info( chalk.bold('\nUser:'), chalk.blue.inverse(msg.author.username + '#' + msg.author.discriminator));
    if (msg.channel.type != "dm") console.log(chalk.bold('Guild:'), chalk.yellow.inverse(msg.guild.name));
    console.log(chalk.bold('Command:'), chalk.green.inverse(command), chalk.bold('\nDate: ') + chalk.cyan.inverse(dayjs().format() + '\n'));
    if (!bot.commands.has(command)) return;

    try {
      if (msg.channel.type != "dm") {
        msg.delete();
      }
      bot.commands.get(command).execute(msg, args, bot);
    //  const guild_id = msg.guild;
    //  const member_id = msg.id;
  //    let guild = bot.commands.find(guild => guild.id === guild_id);
    //  console.log(guild);
    } catch (error) {
      console.error(error);
      // TODO: Replace this error message to templates main error
      const message = {
        color: 0xff0000,
        title: 'Valami félrement',
        description: 'Sajnos a kérésedre most nem tudok értékelhető választ adni.',
        timestamp: dayjs().format(),
        footer: {
          text: 'Generálva általam',
          icon_url: process.env.avatarUrl,
        },
      };
      msg.channel.send({ embed: message});
    }
  }
});

// TODO: Create guildMember remove action
bot.on('guildMemberAdd', async member => {
    const guild = member.guild;
    guild.systemChannel.bulkDelete(1);


    let avatarUrl = await member.user.displayAvatarURL({ format: 'jpg' })
    const message = {
         title: 'Új tag',
        	author: {
        		name: member.user.username + '#' + member.user.discriminator,
        		icon_url: avatarUrl
        	},
        	description: ':inbox_tray: <@' + member.user.id + '> **csatlakozott a szerverhez**',
        	thumbnail: {
        		url: avatarUrl
        	},
          fields: [
        		{
        			name: 'Regisztrált',
        			value: dayjs(member.joinedTimestamp).format('YYYY-HH-MM'),
        		}
          ],
          color: 0x7cfc00,
          timestamp: dayjs().format(),
          footer: {
            text: 'ID: ' + member.user.id
          }
        };

    if(guild.systemChannel){ // Checking if it's not null
      guild.systemChannel.send({ embed: message});
    }
    //newUsers[guild.id].clear();
});

bot.on("guildCreate", guild => {
  const exampleEmbed = {
    color: 0x0099ff,
    title: 'Információ',
    url: 'https://derimiksa.hu',
    fields: [
      {
        name: 'Kedves Barátom!',
        value: 'Tudom, hogy most nehéz idő jár ránk eme fránya fertőzéshullám miatt.' +
        '\nAzért csatlakoztam ehhez az ördögi módon működő rendszerhez, hogy ezzel is segítsem az iskolába járó diákjaim tanulását.' +
        '\nNem fogok beleavatkozni a társalgásukba, csak külső szemlélődő leszek.' +
        '\nAmennyiben szeretnék igénybe venni a segítségemet, ahhoz szükséges kommandokat kell megadni:',
      },
      {
        name: '\u200b',
        value: '\u200b',
        inline: false,
      },
      {
        name: '`?help`',
        value: 'Az alábbi üzenetet jelníti meg',
        inline: true,
      },
      {
        name: '`?korona`',
        value: 'Lekéri a jelenlegi statisztikákat',
        inline: true,
      },
      {
        name: '`?ping`',
        value: 'Erre egy pong üzenettel válaszolok',
        inline: true,
      },
      {
        name: '`?random topic`',
        value: 'A topic helyet beírsz egy általad válaszott témát és random küld egy gif-et.',
        inline: true,
      },
      {
        name: '`?v`',
        value: 'Különböző effektek bejátszása az adott voice channel-en.',
        inline: true,
      },
      {
        name: '\u200b',
        value: '\u200b',
        inline: false,
      },
      {
        name: 'Támogatás',
        value: '[Ha tetszenek a funkcióim, akkor kérlek támogasd egy kávéval a készítőmet.](https://www.buymeacoffee.com/d3v \'Támogass egy kávéval\')',
        inline: false,
      }
    ],
    image: {
      url: 'https://cdn.buymeacoffee.com/buttons/lato-red.png'
    },
    timestamp: dayjs().format(),
    footer: {
      text: 'Generálva általam',
      icon_url: process.env.avatarUrl,
    }
  };
  if(guild.systemChannel){ // Checking if it's not null
    guild.systemChannel.send({ embed: exampleEmbed });
  }

  const toAdmin = {
    color: 0xffc300,
    title: 'Új szerver a kezemben :grinning: ',
    fields: [
    {
      name: 'Név',
      value: guild.name,
      inline: true,
    },
    {
      name: 'Guild id',
      value: guild.id,
      inline: true,
    },
    {
      name: 'Felhasználók száma',
      value: guild.memberCount,
      inline: true,
    }
    ],
    timestamp: dayjs().format(),
    footer: {
      text: 'Generálva általam',
      icon_url: process.env.avatarUrl,
    }
  }
  bot.users.cache.get('342836263541276673').send({ embed: toAdmin });

})

app.get('/', function(req, res) {
    res.send('OK');
});

app.listen(port);
